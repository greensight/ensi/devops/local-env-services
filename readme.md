# Ensi local environment services #

```shell
./make-env

./network up

# запуск по отдельности
docker-compose up -d postgres
docker-compose up -d kafka
docker-compose up -d kibana

# остановка всех сервисов
docker-compose down
```

Сервисы доступны в сети docker по адресам:

- postgres:5432
- kafka:9092
- elastic:9200
- redis:6379

Сервисы доступны на хосте на портах 5432, 9092, 9200 и 6379 соответственно.

Кибана доступна в браузере по адресу http://kibana.ensi.127.0.0.1.nip.io
Redis-commander доступен в бразузере по адресу http://redis.ensi.127.0.0.1.nip.io
Kafka-UI доступен в браузере по адресу http://kafka.ensi.127.0.0.1.nip.io

## Local tools ##

### psql ###

```shell
bin/psql
```

### Kafka console ###

Консольные клиенты к кафке, один для записи в кафку, другой для чтения из неё.
Скрипты являются обёртками над стандартными kafka-console-* клиентами https://kafka.apache.org/quickstart

```shell
bin/kafka-producer topic-name
bin/kafka-consumer topic-name
```

Для работы с кафкой, которая развёрнута на другом сервере, нужно использовать скрипты `bin/kafka-external-producer` и `bin/kafka-external-producer`.
*Важное замечание*: если вы прокидываете порт кафки из кубера, то вам необходимо:

1) добавить в /etc/hosts правило:
```shell
127.0.0.1 kafka-0.kafka-headless.kafka.svc.cluster.local
```
2) подключаться к кафке по адресу kafka-0.kafka-headless.kafka.svc.cluster.local:9092, вот так:
```shell
bin/kafka-external-producer kafka-0.kafka-headless.kafka.svc.cluster.local:9092 topic-name
bin/kafka-external-consumer kafka-0.kafka-headless.kafka.svc.cluster.local:9092 topic-name
```

### Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
